class aboutFavoriteApp {
  String app = 'EskomSePush';
  String name = 'Dan Wells';
  String year = '2015';
  String category = 'The best breakthrough developer App';
}

void main() {
  var string1 = aboutFavoriteApp();
  print('My favourite app is ${string1.app}');

  print('The founder of the App is ${string1.name}');

  print('The year it won the award is ${string1.year}');

  print('the category: ${string1.category}');

  print(string1.app.toUpperCase());
}
